﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nodestatistics : ZMQListener
{
    Dictionary<string, int> _nodeCount;

    new void Start()
    {
        _nodeCount = new Dictionary<string, int>();
        handler = new PWFDelegate(HandleMessage);
        base.Start();
    }

    void Update()
    {
        lock (_nodeCount)
        {
            foreach (var node in _nodeCount)
            {
                int value = node.Value;

                GameObject obj = GameObject.Find("/Node/" + node.Key);
                if (obj)
                {
                    obj.transform.localScale += new Vector3(0.0f, Mathf.Log(value, 10.0f) * 2.0f, 0.0f);
                }
                else
                {
                    Debug.Log(node.Key + ": " + node.Value);
                }
            }
            _nodeCount.Clear();
        }
    }

    private void HandleMessage(PWFParsed p)
    {
        string name = p.nodename;
        lock (_nodeCount)
        {
            if (_nodeCount.ContainsKey(name))
            {
                _nodeCount[name] += 1;
            }
            else
            {
                _nodeCount[name] = 1;
            }
        }
    }
}
