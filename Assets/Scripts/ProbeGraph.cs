﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class ProbeGraph : ZMQListener
{

    public class NodeObserver : IEquatable<NodeObserver>
    {
        public string name;
        public float seen;

        public NodeObserver(string name, float seen)
        {
            this.name = name;
            this.seen = seen;
        }
        public bool Equals(NodeObserver other)
        {
            return name == other.name;
        }
    }
    public class Probelino : IEquatable<Probelino>
    {
        public string mac;
        public List<string> ssids = new List<string>();
        public List<NodeObserver> nodes = new List<NodeObserver>();

        public float lastSeen = 0f;

        public bool Equals(Probelino other)
        {
            return mac == other.mac;
        }

        public Probelino(string mac)
        {
            this.mac = mac;
        }

        public void TrimNodes(float deadLine)
        {
            for (int i = nodes.Count - 1; i >= 0; i--)
            {
                if (nodes[i].seen < deadLine)
                    nodes.RemoveAt(i);
            }
        }
    }


    List<Probelino> _probelinos;
    public GameObject probePrefab;

    Dictionary<string, Probe> _probes;
    Dictionary<string, GameObject> _nodes;

    public float TTL = 10f;

    public float minDistance = 2f;
    public float maxDistance = 6f;
    new void Start()
    {
        _probes = new Dictionary<string, Probe>();
        _nodes = new Dictionary<string, GameObject>();
        _probelinos = new List<Probelino>();
        handler = new PWFDelegate(HandleMessage);
        base.Start();
    }

    float _now;
    void Update()
    {
        lock (_probelinos)
        {
            _now = Time.time;
            Probelino probe;
            for (int i = _probelinos.Count - 1; i >= 0; i--)
            {
                probe = _probelinos[i];
                probe.TrimNodes(_now - TTL);
                if (_probes.ContainsKey(probe.mac))
                {
                    _probes[probe.mac].Refresh(probe.nodes);
                }
                else
                {
                    if (!_nodes.ContainsKey(probe.nodes[0].name))
                    {
                        _nodes.Add(probe.nodes[0].name, GameObject.Find("/Node/" + probe.nodes[0].name));
                    }
                    GameObject p = Instantiate(probePrefab, GetSafePosition(_nodes[probe.nodes[0].name].transform.position), Quaternion.identity);
                    p.GetComponent<Renderer>().material.SetColor("_EmissionColor", GetRandomColor());
                    _probes.Add(probe.mac, p.GetComponent<Probe>());
                }
                if (probe.nodes.Count == 0)
                {
                    _probelinos.RemoveAt(i);
                    Destroy(_probes[probe.mac].gameObject);
                    _probes.Remove(probe.mac);
                }
            }
        }
    }

    Color GetRandomColor()
    {
        Vector3 c = UnityEngine.Random.insideUnitSphere;
        return new Color(Mathf.Abs(c.x), Mathf.Abs(c.y), Mathf.Abs(c.z));
    }


    Vector3 GetSafePosition(Vector3 center)
    {
        Vector3 sphere = UnityEngine.Random.onUnitSphere * UnityEngine.Random.Range(minDistance, maxDistance); ;
        sphere = new Vector3(sphere.x, Mathf.Clamp(Mathf.Abs(sphere.y), 1, minDistance), sphere.z);

        return center + sphere;
    }

    void HandleMessage(PWFParsed p)
    {
        if (p.type == "wifiprobe")
        {
            lock (_probelinos)
            {
                Probelino prb = new Probelino(p.mac);
                prb.lastSeen = _now;
                NodeObserver node = new NodeObserver(p.nodename, _now);
                prb.nodes.Add(node);
                prb.ssids.Add(p.ssid);
                if (!_probelinos.Contains(prb))
                {
                    _probelinos.Add(prb);
                }
                else
                {
                    prb = _probelinos[_probelinos.LastIndexOf(prb)];
                    //if (!prb.nodes.Contains(node))
                    prb.nodes.Add(node);
                    //if (!prb.ssids.Contains(p.ssid))
                    prb.ssids.Add(p.ssid);
                }
            }
        }
    }

}
