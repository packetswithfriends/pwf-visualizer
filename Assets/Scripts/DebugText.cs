﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugText : ZMQListener
{

    public UnityEngine.UI.Text text;
    public List<string> textLines;
    public int textLineMax = 40;
    string _debugLine = "";

    new void Start()
    {
        handler = new PWFDelegate(HandleMessage);
        base.Start();
    }

    void Update()
    {
        lock (_debugLine)
        {
            if (text != null)
                text.text = _debugLine;
        }
    }

    void HandleMessage(PWFParsed p)
    {
        textLines.Add(p.ToJSON());
        while (textLines.Count > textLineMax)
        {
            textLines.RemoveAt(0);
        }
        lock (_debugLine)
        {
            _debugLine = string.Join("\n", textLines);
        }
    }
}
