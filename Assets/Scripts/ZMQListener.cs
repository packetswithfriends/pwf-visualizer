﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZMQListener : MonoBehaviour
{
    protected GameObject zmqConnector;
    protected ZMQConnection zmqConnection;
    protected PWFDelegate handler;

    protected void Start()
    {
        zmqConnector = GameObject.FindWithTag("ZMQ"); // could be better, findtag is slow
        if (zmqConnector != null)
        {
            zmqConnection = zmqConnector.GetComponent<ZMQConnection>();
            if (handler != null)
            {
                zmqConnection.registerDelegate(handler);
            }
            else
            {
                Debug.Log("Assign a handler in Start()");
            }
        }
        else
        {
            Debug.Log("Start from Main scene");
        }
    }

    void OnDestroy()
    {
        if (zmqConnection != null && handler != null)
        {
            zmqConnection.unregisterDelegate(handler);
        }
    }
}
