﻿//using System;
using System.Threading;
using System.Collections.Generic;
using NetMQ;
using NetMQ.Sockets;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class PWFParsed
{
    //    public string workerid;
    //    public string protocolv;
    public string ifacename;
    public string datalink;
    public string type;
    public string ssid;
    public string mac;
    public string nodename;
    public string caps;
    public int chfreq;

    public static PWFParsed FromJSON(string jsonString)
    {
        return JsonUtility.FromJson<PWFParsed>(jsonString);
    }
    public string ToJSON()
    {
        return JsonUtility.ToJson(this);
    }
}

public delegate void PWFDelegate(PWFParsed message);

public class PWFSubscriber
{
    private readonly Thread _worker;

    private bool _listenerCancelled;

    private readonly PWFDelegate _pwfDelegate;

    private void ListenerWorker()
    {
        AsyncIO.ForceDotNet.Force();
        using (var sub = new SubscriberSocket())
        {
            sub.Options.ReceiveHighWatermark = 1000;
            sub.Connect("tcp://192.168.24.205:5066");
            sub.Subscribe("");
            UnityEngine.Debug.Log("Subscriber socket connecting...");
            while (!_listenerCancelled)
            {
                List<string> messages = sub.ReceiveMultipartStrings();
                //Debug.Log(messages[messages.Count - 1]);
                PWFParsed p = PWFParsed.FromJSON(messages[messages.Count - 1]);
                _pwfDelegate(p);
            }
        }

        NetMQConfig.Cleanup(false);
    }

    public PWFSubscriber(PWFDelegate pwfDelegate)
    {
        _pwfDelegate = pwfDelegate;
        _worker = new Thread(ListenerWorker);
    }

    public void Start()
    {
        _listenerCancelled = false;
        _worker.Start();
    }

    public void Stop()
    {
        _listenerCancelled = true;
        _worker.Join();
    }
}

public class ZMQConnection : MonoBehaviour
{
    private PWFSubscriber _pwfsubscriber;
    private Queue<PWFParsed> _messageQueue;
    private List<PWFDelegate> _delegates;

    public void registerDelegate(PWFDelegate _delegate)
    {
        UnityEngine.Debug.Log("registering delegate: " + _delegate.GetHashCode());
        _delegates.Add(_delegate);
    }

    public void unregisterDelegate(PWFDelegate _delegate)
    {
        UnityEngine.Debug.Log("unregistering delegate: " + _delegate.GetHashCode());
        _delegates.Remove(_delegate);
    }

    void Awake()
    {
        _delegates = new List<PWFDelegate>();
        DontDestroyOnLoad(this);
    }

    int _currentSceneIndex;
    int _sceneCount;

    private void Start()
    {
        _messageQueue = new Queue<PWFParsed>();
        _pwfsubscriber = new PWFSubscriber(HandleMessage);
        _pwfsubscriber.Start();
        _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        _sceneCount = SceneManager.sceneCountInBuildSettings;
    }

    private void Update()
    {
        /*
        if(_messageQueue.Count > 0) {
            PWFParsed p = _messageQueue.Dequeue();
            Debug.Log("Message in main: " + p.ssid);

            textLines.Add(p.ToJSON());
        }
        while(textLines.Count > textLineMax) {
            textLines.RemoveAt(0);
        }*/

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _currentSceneIndex++;
            if (_currentSceneIndex >= _sceneCount)
            {
                _currentSceneIndex = 1;
            }
            SceneManager.LoadScene(_currentSceneIndex);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _currentSceneIndex--;
            if (_currentSceneIndex <= 0)
            {
                _currentSceneIndex = _sceneCount - 1;
            }
            SceneManager.LoadScene(_currentSceneIndex);
        }
    }

    private void HandleMessage(PWFParsed p)
    {
        foreach (var _delegate in _delegates)
        {
            _delegate(p);
        }
        //        _messageQueue.Enqueue(message);
    }

    private void OnDestroy()
    {
        _pwfsubscriber.Stop();
    }

}
