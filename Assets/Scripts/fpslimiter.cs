﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fpslimiter : MonoBehaviour
{

    public int editorVSyncCount = 0;
    public int editorTargetFrameRate = 30;
    public int runtimeVSyncCount = 1;
    public int runtimeTargetFrameRate = 30;
     void Awake () {
        #if UNITY_EDITOR
        QualitySettings.vSyncCount = editorVSyncCount;
        Application.targetFrameRate = editorTargetFrameRate;
        #else
        QualitySettings.vSyncCount = runtimeVSyncCount;
        Application.targetFrameRate = runtimeTargetFrameRate;
        #endif
    }
}
