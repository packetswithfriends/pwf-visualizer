﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Probe : MonoBehaviour
{
    // Start is called before the first frame update

    public float minDistance = 2f;
    public float sizeLerpResponse = 0.01f;

    public List<GameObject> nodes;

    Vector3 originalPos;

    void Start()
    {
        nodes = new List<GameObject>();
        originalPos = transform.position;

    }

    Vector3 target;

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target, sizeLerpResponse * Time.deltaTime);
    }

    Vector3 Wobble() {
        Vector3 sphere = UnityEngine.Random.onUnitSphere * UnityEngine.Random.Range(minDistance, 2 * minDistance);
        sphere = new Vector3(sphere.x, Mathf.Clamp01(Mathf.Abs(sphere.y)), sphere.z);
        return sphere;
    }

    Vector3 GetSafePosition(Vector3 center)
    {
        Vector3 sphere = UnityEngine.Random.onUnitSphere * UnityEngine.Random.Range(minDistance, 2 * minDistance); ;
        sphere = new Vector3(sphere.x, Mathf.Clamp(Mathf.Abs(sphere.y), 1, minDistance), sphere.z);

        return center + sphere;
    }

    public void Refresh(List<ProbeGraph.NodeObserver> nodeinfos)
    {
        nodes = new List<GameObject>();
        foreach (ProbeGraph.NodeObserver nodeinfo in nodeinfos)
        {
            nodes.Add(GameObject.Find("/Node/" + nodeinfo.name));
        }

        // calculate target position equidistant of all seen nodes
        if (nodes.Count > 1)
        {
            target = Vector3.zero;
            foreach (GameObject node in nodes)
            {
                target += node.transform.position;
            }
            target /= nodes.Count;
            target += Wobble();
        }
        else if (nodes.Count == 1)
        {
            target = GetSafePosition(nodes[0].transform.position) + Wobble();
        }
        else {
            target = transform.position + Wobble();
        }

    }
}
