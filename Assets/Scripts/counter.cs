﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class counter : ZMQListener
{
    public int packetCount = 0;

    public UnityEngine.UI.Text text;

    new void Start()
    {
        handler = new PWFDelegate(HandleMessage);
        base.Start();
    }

    void Update()
    {
        text.text = "" + packetCount;
    }

    void HandleMessage(PWFParsed p)
    {
        packetCount += 1;

    }
}
