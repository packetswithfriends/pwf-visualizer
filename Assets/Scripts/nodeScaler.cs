﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nodeScaler : MonoBehaviour
{
    public float sizeLerpResponse = 0.1f;
    public Vector3 position2;

    void Start()
    {
        position2 = transform.position;
    }

    void Update()
    {
        transform.localScale = Vector3.Lerp(
            transform.localScale,
            new Vector3(0.8f, 0.8f, 0.8f),
            sizeLerpResponse
        );
    }

    void LateUpdate()
    {
        transform.position = new Vector3(position2.x, transform.localScale.y / 2f, position2.z);
    }
}
